// console.log("Hello World");

let firstName = "Enrico";
let lastName = "Dela Rosa";
let age = 26;
let hobbies = ["Running", "Swimming", "Biking"];
let workObj = {
	houseNumber: 6,
	street: "Farmer's Avenue",
	city: "Pasig City",
	state: "Metro Manila"
}


const log = t => {
	console.log(t); 
}

log("First Name: " + firstName);
log("Last Name: " + lastName);
log("Age: " + age);
log("Hobbies:");
log(hobbies);
log("Work Address:");
log(workObj);

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:




/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	

	let fullName = "Steve Rogers";
	console.log("My full name is: " + fullName);

	let newAge = 40;
	console.log("My current age is: " + newAge);
	
	let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
	console.log("My Friends are: ")
	console.log(friends);

	let profile = {
		username: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false,

	}
	console.log("My Full Profile: ")
	console.log(profile);

	fullName = "Bucky Barnes";
	console.log("My bestfriend is: " + fullName);

	let lastLocation = "Arctic Ocean";
	lastLocation = "Atlantic Ocean";
	console.log("I was found frozen in: " + lastLocation);
